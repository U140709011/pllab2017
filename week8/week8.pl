use strict;
use warnings;

#EXAMPLE OF INPUT USAGE
print " Enter your name:" ;
my $name = <>;
chomp $name;

print "Enter your age: ";
my $age = <>;
chomp $age;

print " Hello, $name! " ;

print " On your next birthday, you will be " , $age+1 , ".\n";

#EXAMPLE OF PRINTF USAGE
my $p = 10/7;
printf "I have written %2.1f programs .\n", $p;
printf "I have written %2.1f programs .\n", $p;
printf "I have written %0.2f programs .\n", $p;

#EXAMPLE OF TRUE FALSE
my $x = 25 ;
if ($x)  {print "The value of \$x = $x\n;" }
else{print "\$x is undefined  or equal to 0 or empty string .\n";}

#EXAMPLE OF TRUE FALSE
my @a = ("Ali");
if (@a) {print "The length of \@a = ". scalar @a . "\n";}
else {print "\@a is undefined or empty \n";}

my $str = "Hello, world!";

$x = substr $str, 0 ,8 ;
print "$x\n";
#from the 7th index get 5 characters more!
substr $str, 7, 5 , "$name";
print "$str\n";

exit;
